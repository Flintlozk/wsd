import { Component } from '@angular/core';

import { timer } from 'rxjs';
import { share } from 'rxjs/operators';


@Component({
  selector: 'app-clock',
  templateUrl: './clock.component.html',
  styleUrls: ['./clock.component.css']
})
export class ClockComponent {

  public clock: Date;

  constructor() {
    timer(1000, 1000).pipe(share()).subscribe(() => this.clock = new Date());
  }
}
