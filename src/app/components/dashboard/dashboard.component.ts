import { Component, OnInit } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  members = [
    {
      name: 'PEOPLE1',
      queue: [
        {
          jobName: 'Job1',
          description: 'Lorem Ipsum is simply dummy text of the printing ...'
        },
        {
          jobName: 'Job3',
          description: 'Lorem Ipsum is simply dummy text of the printing ...'
        },
      ],
      doing: [
        {
          jobName: 'Job2',
          description: 'Lorem Ipsum is simply dummy text of the printing ...'
        },
      ],
      done: []
    },
    {
      name: 'PEOPLE2',
      queue: [
        {
          jobName: 'Job1',
          description: 'Lorem Ipsum is simply dummy text of the printing ...'
        },
        {
          jobName: 'Job3',
          description: 'Lorem Ipsum is simply dummy text of the printing ...'
        },
      ],
      doing: [
      ],
      done: [
        {
          jobName: 'JOB ABC',
          description: 'Lorem Ipsum is simply dummy text of the printing ...'
        },
        {
          jobName: 'JOB YXZ',
          description: 'Lorem Ipsum is simply dummy text of the printing ...'
        },
      ]
    }
  ];


  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }

  constructor() { }

  ngOnInit() {
  }
}
